#include "seal/seal.h"
#include <iostream>
#include <vector>
#include <iterator>
#include "examples.h"
#include "utils.h"
#include <cmath>
#include <algorithm>
#define squareN N*N

auto beginTime =  std::chrono::steady_clock::now();
auto endTime = std::chrono::steady_clock::now();
auto elapsed = 0;

float matrix_a_size = 0;
float matrix_a_size_plain = 0;
float matrix_a_size_cipher = 0;
float matrix_a_size_plain_total = 0;
float matrix_a_size_cipher_total = 0;

using namespace std;
using namespace seal;

template<typename T, size_t n>
void construct_u_sigma(T (& u_sigma)[n], int d){
    for(int l=0; l<pow(d, 2); l++){
        for(int i=0; i<d; i++){
            for(int j=0; j<d; j++){
                int k = d*i +j;
                if(d*i + (i+j)%d == l){
                    u_sigma[k][l] = 1;
                }
                else{
                    u_sigma[k][l] = 0;
                }
            }
        }
    }
}


template<typename T, size_t n>
void construct_u_tau(T (& u_tau)[n], int d){
    for(int l=0; l<pow(d, 2); l++){
        for(int i=0; i<d; i++){
            for(int j=0; j<d; j++){
                int k = d*i +j;
                if( ((i+j)%d)*d + j == l){
                    u_tau[k][l] = 1;
                }
                else{
                    u_tau[k][l] = 0;
                }
            }
        }
    }
}

template<typename T, size_t n>
void construct_a_vector(T (& a_vector)[n], int d){
    int a_index = 0;     
        for(int i=0; i<d; i++){
            for(int j=0; j<d; j++){
                a_vector[a_index] = matrix_global_a[i][j];
                a_index++;
            }
        }   
}

template<typename T, size_t n>
void construct_b_vector(T (& b_vector)[n], int d){
    int b_index = 0;     
        for(int i=0; i<d; i++){
            for(int j=0; j<d; j++){
                b_vector[b_index] = matrix_global_b[i][j];
                b_index++;
            }
        }   
}

template<typename T, size_t n>
Ciphertext constructZeroCiphertext(T const (& u_vector)[n], T const (& diagonals)[n][n], SEALContext context, PublicKey public_key, SecretKey secret_key, GaloisKeys galois_keys, RelinKeys relin_keys, EncryptionParameters parms){
    
    Encryptor encryptor(context, public_key);
    Evaluator evaluator(context);
    BatchEncoder batch_encoder(context);
    Decryptor decryptor(context, secret_key);

    size_t slot_count = batch_encoder.slot_count();    

    vector<uint64_t> vector_to_encode = createVectorToEncode(u_vector, slot_count);

    Plaintext plaintext_vector;    
    batch_encoder.encode(vector_to_encode, plaintext_vector);       
    Ciphertext encrypted_vector; 
    encryptor.encrypt(plaintext_vector, encrypted_vector);

    Ciphertext previousResult; 
    Ciphertext zero_matrix;   

    for(int i=0; i<n; i++){              
        vector<uint64_t> matrix_row_to_encode(slot_count, 0ULL); 

        for(int p=0; p<n; p++){            
            matrix_row_to_encode[p] = diagonals[i][p];          
        }          
        
        Plaintext plaintext_matrix_row;    
        batch_encoder.encode(matrix_row_to_encode, plaintext_matrix_row); 
        encryptor.encrypt(plaintext_matrix_row, zero_matrix);


        /* ~~ MEASURE SIZE */  

        matrix_a_size_plain = measurePlaintextSize(plaintext_matrix_row, parms);
        matrix_a_size_plain_total = matrix_a_size_plain_total + matrix_a_size_plain;  

        /* ~~ MEASURE SIZE END */ 

        // multiply the rotated vector with the row
        evaluator.multiply_inplace(zero_matrix, encrypted_vector);    
        // rotate by 1 slot to the left
        evaluator.rotate_rows_inplace(encrypted_vector, 1, galois_keys);    
        if(i!=0){   
            evaluator.add_inplace(zero_matrix, previousResult);      
        }
        evaluator.relinearize_inplace(zero_matrix, relin_keys);


        if(DEBUG_MODE){              
            cout << "    + Noise budget in zero_matrix: " << decryptor.invariant_noise_budget(zero_matrix) << " bits"
            << endl;     
        }  

        previousResult = zero_matrix;

    }

        /* ~~ MEASURE SIZE */      

        matrix_a_size = N*N*sizeof(int);

        matrix_a_size_cipher = measureCiphertextSize(zero_matrix, parms);        
        matrix_a_size_cipher_total = matrix_a_size_cipher;

        /* ~~ MEASURE SIZE END */ 

    return zero_matrix;   
}

Ciphertext extendZeroCiphertext(Ciphertext zero, SEALContext context, PublicKey public_key, GaloisKeys galois_keys, int shiftByN){

    Encryptor encryptor(context, public_key);
    Evaluator evaluator(context);
    BatchEncoder batch_encoder(context);

    size_t slot_count = batch_encoder.slot_count();

    vector<uint64_t> vector_to_encode(slot_count, 0ULL);

    Plaintext plaintext_vector;  

    batch_encoder.encode(vector_to_encode, plaintext_vector);

    Ciphertext double_zero;
    encryptor.encrypt(plaintext_vector, double_zero);

    // create a copy of zero matrix
    evaluator.add_inplace(double_zero, zero);

    evaluator.rotate_rows_inplace(double_zero, shiftByN*(-1), galois_keys); 

    evaluator.add_inplace(double_zero, zero);

    return double_zero;
}

Ciphertext transformMatrixA(Ciphertext a0, SEALContext context, PublicKey public_key, GaloisKeys galois_keys, int shiftByN, RelinKeys relin_keys){

    Encryptor encryptor(context, public_key);
    Evaluator evaluator(context);
    BatchEncoder batch_encoder(context);

    size_t slot_count = batch_encoder.slot_count();    
     
    vector<uint64_t> part1_vector(slot_count, 0ULL);

    for(int i=0; i<squareN; i++){
        if((i+1)%N==0){            
            part1_vector[i] = 1;
        }
    }

    Plaintext part1_plaintext;    
    batch_encoder.encode(part1_vector, part1_plaintext);           
    Ciphertext cipher_part1;
    encryptor.encrypt(part1_plaintext, cipher_part1);

    vector<uint64_t> part2_vector(slot_count, 1ULL);

    for(int i=0; i<squareN; i++){
        if((i+1)%N==0){            
            part2_vector[i] = 0;
        }
    }

    Plaintext part2_plaintext;    
    batch_encoder.encode(part2_vector, part2_plaintext);      
    Ciphertext cipher_part2; 
    encryptor.encrypt(part2_plaintext, cipher_part2);

    vector<uint64_t> result_vector(slot_count, 0ULL);    

    Plaintext plaintext_result;    
    batch_encoder.encode(result_vector, plaintext_result);   
    Ciphertext result;  
    encryptor.encrypt(plaintext_result, result);

    evaluator.rotate_rows_inplace(a0, (N-1)*(-1), galois_keys);   

    evaluator.multiply_inplace(cipher_part1, a0);

    evaluator.relinearize_inplace(cipher_part1, relin_keys);

    evaluator.rotate_rows_inplace(a0, N, galois_keys);  

    evaluator.multiply_inplace(cipher_part2, a0);

    evaluator.relinearize_inplace(cipher_part2, relin_keys);

    evaluator.add_inplace(result, cipher_part1);

    evaluator.add_inplace(result, cipher_part2);

    return result;
}

void multiply_matrices(){

    printDivider(3);

    cout << " OPTIMIZED MULTIPLICATION BETWEEN 2 MATRICES " << endl << endl;

    static int u_sigma [squareN][squareN];

    static int u_tau [squareN][squareN];

    construct_u_sigma(u_sigma, N);
    construct_u_tau(u_tau, N);

    if(PRINT_MATRICES){

        cout << "U_Sigma: " << endl << endl;    

        print_matrix_own(u_sigma);

        cout << endl;

        cout << endl << "U_Tau: " << endl << endl;
        
        print_matrix_own(u_tau);

        cout << endl;

    }    

    static int a_vector [squareN];

    static int b_vector [squareN];

    construct_a_vector(a_vector, N);
    construct_b_vector(b_vector, N);

    if(PRINT_MATRICES){
        cout << "A: " << endl << endl;

        print_vector(a_vector);

        cout << endl;

        cout << "B: " << endl << endl;

        print_vector(b_vector);

        cout << endl;
    }    

    // sigma diagonals 

    static int u_sigma_diagonals[squareN][squareN];    

    createDiagonalsMatrix(u_sigma, u_sigma_diagonals); 

    if(PRINT_MATRICES){
        cout << endl;

        cout << "U_Sigma Diagonal Matrix: " << endl << endl;

        print_matrix_own(u_sigma_diagonals);

        cout << endl;
    } 

    // tau diagonals

    static int u_tau_diagonals[squareN][squareN];

    createDiagonalsMatrix(u_tau, u_tau_diagonals);

    if(PRINT_MATRICES){
        cout << endl;

        cout << "U_Tau Diagonal Matrix: " << endl << endl;

        print_matrix_own(u_tau_diagonals);

        cout << endl;
    }    

    // PARAMETERS   

    EncryptionParameters parms(scheme_type::bfv);
    size_t poly_modulus_degree = 8192;

    if(N>32){
        poly_modulus_degree = 16384;
    }

    parms.set_poly_modulus_degree(poly_modulus_degree);
    parms.set_coeff_modulus(CoeffModulus::BFVDefault(poly_modulus_degree));
    parms.set_plain_modulus(PlainModulus::Batching(poly_modulus_degree, 20));
    SEALContext context(parms);

    auto qualifiers = context.first_context_data()->qualifiers();
    cout << "Batching enabled: " << boolalpha << qualifiers.using_batching << endl;

    cout << "SEAL Parameter validation (success): " << context.parameter_error_message() << endl << endl;

    KeyGenerator keygen(context);
    SecretKey secret_key = keygen.secret_key();
    PublicKey public_key;
    keygen.create_public_key(public_key);
    RelinKeys relin_keys_1;
    keygen.create_relin_keys(relin_keys_1);
    Encryptor encryptor(context, public_key);
    Evaluator evaluator(context);
    Decryptor decryptor(context, secret_key);
    GaloisKeys galois_keys;
    keygen.create_galois_keys(galois_keys);
    BatchEncoder batch_encoder(context);

    // PARAMETERS END
    
    Ciphertext a0 = constructZeroCiphertext(a_vector, u_sigma_diagonals, context, public_key, secret_key, galois_keys, relin_keys_1, parms);    
    Ciphertext b0 = constructZeroCiphertext(b_vector, u_tau_diagonals, context, public_key, secret_key, galois_keys, relin_keys_1, parms);

    b0 = extendZeroCiphertext(b0, context, public_key, galois_keys, squareN);    

    if(DEBUG_MODE){
        Plaintext plain_result;
        vector<uint64_t> pod_result;      
        
        decryptor.decrypt(a0, plain_result);
        batch_encoder.decode(plain_result, pod_result); 

        cout << endl;  

        cout << "A0 (doubled): " << endl;      
                   
        cout << "[ ";
        for(int t=0; t<squareN*2; t++){                
            cout << pod_result[t] << " ";
            if(t==squareN*2-1){
                cout << " ]" << endl;
            }          
        }  

        cout << endl;  

        cout << "B0 (doubled): " << endl; 

        decryptor.decrypt(b0, plain_result);
        batch_encoder.decode(plain_result, pod_result); 
                   
        cout << "[ ";
        for(int t=0; t<squareN*2; t++){                
            cout << pod_result[t] << " ";
            if(t==squareN*2-1){
                cout << " ]" << endl;
            }          
        } 

        cout << endl;  
    }

    size_t slot_count = batch_encoder.slot_count();

    // for the total sum, create a ciphertext of zeros

    vector<uint64_t> sum_vector(slot_count, 0ULL);
    Plaintext sum_plaintext;    
    batch_encoder.encode(sum_vector, sum_plaintext);           
    Ciphertext sum;
    encryptor.encrypt(sum_plaintext, sum);
    

    for(int k=1; k<=N; k++){        

        RelinKeys relin_keys_2;
        keygen.create_relin_keys(relin_keys_2);

        // for holding the temporary component-wise product, create a ciphertext of ones
        vector<uint64_t> temp_vector(slot_count, 1ULL);
        Plaintext temp_plaintext;    
        batch_encoder.encode(temp_vector, temp_plaintext);           
        Ciphertext temp;
        encryptor.encrypt(temp_plaintext, temp);
        
        beginTime = std::chrono::steady_clock::now();

        evaluator.multiply(a0, b0, temp);
        evaluator.relinearize_inplace(temp, relin_keys_1);         

        if(DEBUG_MODE){                 
            cout << "    + Noise budget in temp: " << decryptor.invariant_noise_budget(temp) << " bits"
            << endl;     

            Plaintext plain_temp;
            vector<uint64_t> temp_result;      
            
            decryptor.decrypt(temp, plain_temp);
            batch_encoder.decode(plain_temp, temp_result); 

            cout << endl;   

            cout << "temp (component-wise product of A and B) in iteration #" << k << ":" << endl;      
                    
            cout << "[ ";
            for(int t=0; t<squareN; t++){                
                cout << temp_result[t] << " ";
                if(t==squareN-1){
                    cout << " ]" << endl;
                }          
            }  

            cout << endl;  

        }  

        evaluator.add_inplace(sum, temp);

        if(k==N){
            endTime = std::chrono::steady_clock::now();
            elapsed = elapsed + std::chrono::duration_cast<std::chrono::microseconds>(endTime - beginTime).count();   
            break;
        }

        evaluator.rotate_rows_inplace(b0, N, galois_keys);
        a0 = transformMatrixA(a0, context, public_key, galois_keys, squareN, relin_keys_1);   

        endTime = std::chrono::steady_clock::now();
        elapsed = elapsed + std::chrono::duration_cast<std::chrono::microseconds>(endTime - beginTime).count();          

            if(DEBUG_MODE){
                Plaintext plain_result;
                vector<uint64_t> pod_result;      
                
                decryptor.decrypt(a0, plain_result);
                batch_encoder.decode(plain_result, pod_result); 

                cout << endl;   

                cout << "A" << k << "(doubled): " << endl;      
                        
                cout << "[ ";
                for(int t=0; t<squareN*2; t++){                
                    cout << pod_result[t] << " ";
                    if(t==squareN*2-1){
                        cout << " ]" << endl;
                    }          
                }  

                cout << endl;  

                cout << "B" << k << "(doubled): " << endl;    

                decryptor.decrypt(b0, plain_result);
                batch_encoder.decode(plain_result, pod_result); 
                        
                cout << "[ ";
                for(int t=0; t<squareN*2; t++){                
                    cout << pod_result[t] << " ";
                    if(t==squareN*2-1){
                        cout << " ]" << endl;
                    }          
                } 

                cout << endl;  
        }
    }   

    Plaintext plain_sum;
    vector<uint64_t> pod_sum; 
    cout << "Decrypt and decode result." << endl;
    cout << endl;

    decryptor.decrypt(sum, plain_sum);
    batch_encoder.decode(plain_sum, pod_sum);  

    cout << "Decrypted result of final product: " << endl;
    cout << "[ ";
    int slotCount = 0;
    for(int t=0; t<squareN; t++){                
        cout << pod_sum[t] << " ";
        slotCount ++;       
        if(slotCount==N){
            cout << " ]" << endl;
            slotCount =0;
        }         
    } 
    cout << endl;    
}

int main(){     

    if(!USER_DEFINED_INPUT){
        init_input();
    }      
    
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now(); 
   
    multiply_matrices();    

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    auto totalElapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

    cout << "Time difference without encryption/decryption process = " << elapsed << "[µs]" << std::endl;    
    
    cout << "Time difference total = " << totalElapsed << "[µs]" << std::endl;

    printSizeMeasurements(matrix_a_size, matrix_a_size_plain, matrix_a_size_cipher, matrix_a_size_plain_total, matrix_a_size_cipher_total);

    return 0;
 }

 

