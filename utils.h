#include <iostream>
#include "input.h"
#include "seal/seal.h"

using namespace std;

inline void init_input(){
    for(int i=0; i<N; i++){
        vector_global[i] = i;
        for(int j=0;j<N;j++){
            matrix_global_a[i][j] = i+j;
            matrix_global_b[i][j] = i+j;
        }
    }
}

inline void printDivider(int k){

    cout << endl;

    cout << "~~~~~~ METHOD " << k << " ~~~~~~";

    cout << endl << endl;
}

template<typename T, size_t n>
inline void print_vector(T const(& vector)[n])
{
    cout << "[ ";
    for (size_t i = 0; i < n; i++) {
        std::cout << vector[i] << ' ';
    }
    cout << "]" << endl;
}

template<typename T, size_t n>
inline void print_matrix_own(T const(& matrix)[n])
{    
    for (size_t i = 0; i < n; i++) {
        print_vector(matrix[i]);
    }    
}

template<typename T, size_t n>
void shiftMatrixByK( T const(& oldMatrix)[n][n], T (& newMatrix)[n][n], int k)
{
    if (k > n) {
        cout << "shifting is not possible" << endl;
        return;
    }
     
    int j = 0;
    while (j < n) {
        int h=0; // column index of the new matrix  
        //from index k
        for (int i = k; i < n; i++){
            newMatrix[j][h] = oldMatrix[j][i];           
            h++;
        }            
             
        //before index k
        for (int i = 0; i < k; i++){
            newMatrix[j][h] = oldMatrix[j][i];            
            h++;
        }

        j++;
    }  
}

template<typename T, size_t n>
void getPrincipalDiagonal(T const(& originalMatrix)[n][n], T (& diagonalMatrix)[n][n], int columnIndex)
{      
    for (int i = 0; i < n; i++) {       
        for (int j = 0; j < n; j++) { 
            if (i == j){                
                diagonalMatrix[columnIndex][i] = originalMatrix[i][j];
            }             
        }
    }        
}

template<typename T, size_t n>
void createDiagonalsMatrix(T const(& originalMatrix)[n][n], T (& diagonalMatrix)[n][n])
{ 

    static int a[n][n];    

    static int aa[n][n];

    int k = 1;

    memcpy (a, originalMatrix, n*n*sizeof(int));

    memcpy (aa, a, n*n*sizeof(int));

    memcpy (diagonalMatrix, a, n*n*sizeof(int));

    if(PRINT_MATRICES){
        cout << "Original matrix: " << endl;
        cout << endl;
        print_matrix_own(a);	
        cout << endl;
    }    

    for(int i=0; i<n; i++){
        getPrincipalDiagonal(aa, diagonalMatrix, i);
        if(i==n) break;        
        shiftMatrixByK(a, aa, k);
        k++;       
    }       
}

template<typename T, size_t n>
vector<uint64_t> createVectorToEncode(T const(& originalVector)[n], size_t slot_count){

    static int vector_sample[n];

    vector<uint64_t> vector_to_encode(slot_count, 0ULL);

    memcpy (vector_sample, originalVector, n*sizeof(int));     

    // double the vector so that the slots occupied are n*2
    for(int l=0; l<n*2; l++){
        if(l<n){
            vector_to_encode[l] = vector_sample[l];            
        }
        else{
            vector_to_encode[l] = vector_sample[l-n];
        }        
    }    

    return vector_to_encode;
}

float measureCiphertextSize(seal::Ciphertext ciphertext, seal::EncryptionParameters parms){ 
    
    int byteSize = 0;     
    
    byteSize = ciphertext.size() * parms.coeff_modulus().size() * parms.poly_modulus_degree() * 8;

    return byteSize/1000;
}

float measurePlaintextSize(seal::Plaintext plaintext, seal::EncryptionParameters parms){ 
    
    int byteSize = 0;     
    
    byteSize = parms.coeff_modulus().size() * parms.poly_modulus_degree() * 8;

    return byteSize/1000;
}

inline void printSizeMeasurements(float matrix_a_size, float matrix_a_size_plain, float matrix_a_size_cipher, float matrix_a_size_plain_total, float matrix_a_size_cipher_total){

    cout << endl;

    cout << "Size of matrix A before encoding : " << matrix_a_size/1000 << " kB" << std::endl;
    cout << "Size of matrix A single plaintext : " << matrix_a_size_plain << " kB"  << std::endl;
    cout << "Size of matrix A single ciphertext : " << matrix_a_size_cipher << " kB"  << std::endl;   
    //cout << "Total size of matrix A plaintexts : " << matrix_a_size_plain_total << " kB"  << std::endl;
    cout << "Size of total number of matrix A ciphertexts : " << matrix_a_size_cipher_total << " kB"  << std::endl;  
}



