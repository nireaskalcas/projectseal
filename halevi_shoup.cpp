
#include "seal/seal.h"
#include <iostream>
#include <vector>
#include <iterator>
#include "examples.h"
#include "utils.h"
#include <cmath>

auto beginTime =  std::chrono::steady_clock::now();
auto endTime = std::chrono::steady_clock::now();
auto elapsed = 0;

float matrix_a_size = 0;
float matrix_a_size_plain = 0;
float matrix_a_size_cipher = 0;
float matrix_a_size_plain_total = 0;
float matrix_a_size_cipher_total = 0;


using namespace std;
using namespace seal;

void encrypt_matrix_halevi_shoup(int diagonalMatrix[][N]){

    EncryptionParameters parms(scheme_type::bfv);
    size_t poly_modulus_degree = 8192;
    parms.set_poly_modulus_degree(poly_modulus_degree);
    parms.set_coeff_modulus(CoeffModulus::BFVDefault(poly_modulus_degree));
    parms.set_plain_modulus(PlainModulus::Batching(poly_modulus_degree, 20));
    SEALContext context(parms);

    auto qualifiers = context.first_context_data()->qualifiers();
    cout << "Batching enabled: " << boolalpha << qualifiers.using_batching << endl;

    KeyGenerator keygen(context);
    SecretKey secret_key = keygen.secret_key();
    PublicKey public_key;
    keygen.create_public_key(public_key);
    RelinKeys relin_keys;
    keygen.create_relin_keys(relin_keys);
    Encryptor encryptor(context, public_key);
    Evaluator evaluator(context);
    Decryptor decryptor(context, secret_key);
    GaloisKeys galois_keys;
    keygen.create_galois_keys(galois_keys);
    BatchEncoder batch_encoder(context);

    cout << "SEAL Parameter validation (success): " << context.parameter_error_message() << endl << endl;

    size_t slot_count = batch_encoder.slot_count();
    size_t row_size = slot_count / 2;

    vector<uint64_t> vector_to_encode = createVectorToEncode(vector_global, slot_count);

    if(DEBUG_MODE){
        cout << "Vector to be encoded: \n" << endl;
        print_matrix(vector_to_encode, row_size); 
        cout << "Encrypt plaintext_vector to encrypted_vector." << endl;
    }
     
    Plaintext plaintext_vector;    
    batch_encoder.encode(vector_to_encode, plaintext_vector);       
    Ciphertext encrypted_vector; 
    encryptor.encrypt(plaintext_vector, encrypted_vector);

    if(DEBUG_MODE){
        cout << "    + Noise budget in encrypted_vector: " << decryptor.invariant_noise_budget(encrypted_vector) << " bits"
         << endl; 
    } 

    Ciphertext previousResult;    

    for(int i=0; i<N; i++){
        vector<uint64_t> matrix_row_to_encode(slot_count, 0ULL); 

        for(int p=0; p<N; p++){
            matrix_row_to_encode[p] = diagonalMatrix[i][p];          
        } 

        if(DEBUG_MODE){
            cout << "Matrix row to be encoded: \n" << endl;
            print_matrix(matrix_row_to_encode, row_size);  
        }           
        
        Plaintext plaintext_matrix_row;    
        batch_encoder.encode(matrix_row_to_encode, plaintext_matrix_row);           
        Ciphertext encrypted_matrix_row;  
        encryptor.encrypt(plaintext_matrix_row, encrypted_matrix_row);

        /* ~~ MEASURE SIZE */      

        matrix_a_size = N*N*sizeof(int);

        matrix_a_size_cipher = measureCiphertextSize(encrypted_matrix_row, parms);
        matrix_a_size_plain = measurePlaintextSize(plaintext_matrix_row, parms);

        matrix_a_size_plain_total = matrix_a_size_plain_total + matrix_a_size_plain;
        matrix_a_size_cipher_total = matrix_a_size_cipher_total + matrix_a_size_cipher;

        /* ~~ MEASURE SIZE END */     

        if(DEBUG_MODE){
            cout << "Encrypt plaintext_matrix_row to encrypted_matrix_row." << endl;        
            cout << "    + Noise budget in encrypted_matrix_row: " << decryptor.invariant_noise_budget(encrypted_matrix_row) << " bits"
            << endl;     
        }  

        beginTime = std::chrono::steady_clock::now();

        // multiply the rotated vector with the row
        evaluator.multiply_inplace(encrypted_matrix_row, encrypted_vector);    
        // rotate by 1 slot to the left
        evaluator.rotate_rows_inplace(encrypted_vector, 1, galois_keys);    
        if(i!=0){   
            evaluator.add_inplace(encrypted_matrix_row, previousResult);      
        }

        endTime = std::chrono::steady_clock::now();
        elapsed = elapsed + std::chrono::duration_cast<std::chrono::microseconds>(endTime - beginTime).count();

        evaluator.relinearize_inplace(encrypted_matrix_row, relin_keys);

        previousResult = encrypted_matrix_row;
        Plaintext plain_result;
        vector<uint64_t> pod_result; 

        if(DEBUG_MODE){
           cout << "Decrypt and decode result." << endl;
           cout << endl;
        }  
        
        decryptor.decrypt(encrypted_matrix_row, plain_result);
        batch_encoder.decode(plain_result, pod_result);                
       
        if(i==N-1){
            cout << "Decrypted result of final product: " << endl << endl;
            cout << "[ ";
            for(int t=0; t<N; t++){                
                cout << pod_result[t] << " ";
                if(t==N-1){
                    cout << " ]" << endl;
                }          
            }     
            cout << endl;                    
        }
    }
}

void multiply_matrix_with_vector_halevi_shoup(){

    printDivider(2);

    cout << " HALEVI & SHOUP " << endl << endl;

    static int diagonalMatrix[N][N];

    createDiagonalsMatrix(matrix_global_a, diagonalMatrix);

    if(PRINT_MATRICES){
        cout << "Diagonals matrix: " << endl;
        print_matrix_own(diagonalMatrix);   
        cout << endl; 
    }    

    encrypt_matrix_halevi_shoup(diagonalMatrix);       
}

int main(){

    if(!USER_DEFINED_INPUT){
        init_input();
    } 

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();   

    multiply_matrix_with_vector_halevi_shoup();

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();    

    auto totalElapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

    cout << "Time difference without encryption/decryption process = " << N*elapsed << "[µs]" << std::endl;    
    
    cout << "Time difference total = " << N*totalElapsed << "[µs]" << std::endl;

    printSizeMeasurements(matrix_a_size, matrix_a_size_plain, matrix_a_size_cipher, matrix_a_size_plain_total, matrix_a_size_cipher_total); 

    return 0;
 }