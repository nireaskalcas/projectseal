

/* 

This file serves as a single input reference for the matrices used in each method. There is the option to let the program
automatically initialize the matrices/vector with integers or edit this file to explicitly enter your own values. 
In the first case you need only to vhange the N value to a dimension you wish to test against the methods, and the code will
take care of initializing the matrices with values (Note USER_DEFINED_INPUT must be 0 for automatic initialization). 

The following options define what should be printed by the program.
PRINT_MATRICES: a value of 0 will skip printing the initial/helper vectors and matrices and only print the decrypted result. Especially useful if N is really large and we only care about the performance metrics. A value of
1 will enable printing.
DEBUG_MODE: Enables printing of additional values such as noise to help troubleshoot for issues.

If you want the matrices to have specific values you need to:

a) Define N with a dimension of your choice (integer value).
b) Change USER_DEFINED_INPUT to 1.
c) Edit this file and enter values for the 2 matrices and 1 vector.

See an example below:

#define N 3

#define DEBUG_MODE 0

#define USER_DEFINED_INPUT 1

int matrix_global_a [N][N] = {{1,2,3},
                              {4,5,6},
                              {7,8,9}};

int matrix_global_b [N][N] = {{11,12,13},
                              {14,15,16},
                              {17,18,19}};

int vector_global [N] = {1,2,3};


 */


#define N 3

#define PRINT_MATRICES 1

#define DEBUG_MODE 0

#define USER_DEFINED_INPUT 0

int matrix_global_a [N][N] = {};

int matrix_global_b [N][N] = {};

int vector_global [N]; 


