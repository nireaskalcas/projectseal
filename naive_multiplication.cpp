#include "seal/seal.h"
#include <iostream>
#include <vector>
#include <iterator>
#include "examples.h"
#include "utils.h"
#include <cmath>
#include <algorithm>

auto beginTime =  std::chrono::steady_clock::now();
auto endTime = std::chrono::steady_clock::now();
auto elapsed = 0;

float matrix_a_size = 0;
float matrix_a_size_plain = 0;
float matrix_a_size_cipher = 0;
float matrix_a_size_plain_total = 0;
float matrix_a_size_cipher_total = 0;

using namespace std::chrono;
using namespace seal;

Ciphertext multiply_add_ciphertexts(SEALContext context, Ciphertext a_cipher, Ciphertext b_cipher, Ciphertext value_to_add){
    Evaluator evaluator(context);
    Ciphertext product_of_a_and_b;
    Ciphertext result;
    evaluator.multiply(a_cipher, b_cipher, product_of_a_and_b);
    evaluator.add(product_of_a_and_b, value_to_add, result);
    return result;
}

template<typename T, size_t n>
int multiply_row_with_vector(T const(& row)[n], int* vector, SEALContext context, PublicKey public_key, SecretKey secret_key, EncryptionParameters parms){

     /* ~~ CREATE ENCRYPTOR & DECRYPTOR */

    Encryptor encryptor(context, public_key);    
    Decryptor decryptor(context, secret_key);

    /* ~~ CREATE ENCRYPTOR & DECRYPTOR (END) */
    
    unsigned int product_sum = 0;
    Plaintext product_sum_plaintext(to_string(0));
    Ciphertext product_sum_cipher;    
    
    encryptor.encrypt(product_sum_plaintext, product_sum_cipher);   

    for (size_t i = 0; i < n; i++) {
        Plaintext a(to_string(row[i]));
        Plaintext b(to_string(vector[i]));
        Ciphertext a_cipher;
        Ciphertext b_cipher;
        encryptor.encrypt(a, a_cipher);
        encryptor.encrypt(b, b_cipher); 

        /* ~~ MEASURE SIZE */      

        matrix_a_size_cipher = measureCiphertextSize(a_cipher, parms);
        matrix_a_size_plain = measurePlaintextSize(a, parms);

        matrix_a_size_plain_total = matrix_a_size_plain_total + matrix_a_size_plain;
        matrix_a_size_cipher_total = matrix_a_size_cipher_total + matrix_a_size_cipher;

        /* ~~ MEASURE SIZE END */     

        beginTime = std::chrono::steady_clock::now();
        product_sum_cipher = multiply_add_ciphertexts(context, a_cipher, b_cipher, product_sum_cipher);   
        endTime = std::chrono::steady_clock::now();
        elapsed = elapsed + std::chrono::duration_cast<std::chrono::microseconds>(endTime - beginTime).count();     
    }
    
    decryptor.decrypt(product_sum_cipher, product_sum_plaintext);    
    
    std::stringstream ss;
    ss << std::hex << product_sum_plaintext.to_string();
    ss >> product_sum;
       
    return product_sum;
}

template<typename T, size_t n>
void match_multiplications(T const(& matrix)[n], int* vector, SEALContext context, EncryptionParameters parms)
{  
    /* ~~ CREATE A SECRET AND PUBLIC KEY */

    KeyGenerator keygen(context);
    SecretKey secret_key = keygen.secret_key();
    PublicKey public_key;
    keygen.create_public_key(public_key);

    /* ~~ CREATE A SECRET AND PUBLIC KEY (END) */   

    static int product_vector [n];
    for (size_t i = 0; i < n; i++){        
        if(DEBUG_MODE == 1){
            cout << "Product of row ";
            print_vector(matrix[i]);
            cout << "with given vector:" << endl;
        }  
        int product_sum = multiply_row_with_vector(matrix[i], vector, context, public_key, secret_key, parms);        
        if(DEBUG_MODE == 1){
            cout << product_sum << endl;
        }        
        product_vector[i] = product_sum;
    }
    cout << "Final result: \n";
    print_vector(product_vector);
}

void multiply_matrix_with_vector_naive(){

    printDivider(1);

    cout << " NAIVE " << endl << endl;

    /* ~~ SETTING SEAL PARAMETERS */

    EncryptionParameters parms(scheme_type::bfv);
    size_t poly_modulus_degree = 4096;
    parms.set_poly_modulus_degree(poly_modulus_degree);
    parms.set_coeff_modulus(CoeffModulus::BFVDefault(poly_modulus_degree));
    parms.set_plain_modulus(1024);  

    SEALContext context(parms);
    
    cout << "SEAL Parameter validation (success): " << context.parameter_error_message() << endl << endl;

    /* ~~ SETTING SEAL PARAMETERS (END) */   

    static int matrix [N][N];
    static int vector[N];

    matrix_a_size = N*N*sizeof(int);

    memcpy (matrix, matrix_global_a, matrix_a_size);
    memcpy (vector, vector_global, N*sizeof(int));

    if(PRINT_MATRICES){
        cout << "Matrix:" << endl;
        print_matrix_own(matrix);
        cout << endl;
        cout << "Vector:" << endl;
        print_vector(vector);  
        cout << endl;
    }
    
    match_multiplications(matrix, vector, context, parms);
}

int main(){  


    if(!USER_DEFINED_INPUT){
        init_input();
    }    

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    multiply_matrix_with_vector_naive();

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    auto totalElapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();

    cout << endl;

    cout << "Time difference without encryption/decryption process = " << N*elapsed << "[µs]" << std::endl;    
    
    cout << "Time difference total = " << N*totalElapsed << "[µs]" << std::endl;
  
    printSizeMeasurements(matrix_a_size, matrix_a_size_plain, matrix_a_size_cipher, matrix_a_size_plain_total, matrix_a_size_cipher_total);

    return 0;
 }